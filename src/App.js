import {
  BrowserRouter as Router,
  Switch,
  Route,
  useHistory,
  Redirect
} from "react-router-dom";

import LoginForm from './pages/login';
import Home from './pages/home';

import { createBrowserHistory } from 'history';

const history = createBrowserHistory({ forceRefresh: true });
function App() {
   // const history = useHistory();

  let token = localStorage.getItem("token");
  console.log('token == ', token)
  return (
    <Router history={history}>
      <Switch>
        {!token ? 
          <>
            <Route exact path="/">
              <LoginForm />
            </Route>
            <Redirect from="*" to="/" />
          </>

          :
          <>
            <Route exact path="/home">
              <Home />
            </Route>
            
            <Redirect from="/" to="/home" />
          </>
        }
      </Switch>
    </Router>
  );
}

export default App;
