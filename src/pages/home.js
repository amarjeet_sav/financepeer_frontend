import {useEffect, useState} from 'react'
import Post from '../components/post'
import axios from 'axios';
import baseUrl from '../constants/baseUrl';
import Header from '../components/header';


const Home = () => {
	let token = localStorage.getItem('token');
	const [posts, setPosts] = useState([]);

	useEffect(() => {
		axios.get(`${baseUrl}/post/list`, { headers: { Authorization: `Token ${token}` }, params: {
    		page: 1, limit: 20} })
		  .then(function (response) {
		    console.log('response == ', response);
		    setPosts(response.data.data)
		  })
		  .catch(function (error) {
		    console.log('error == ', error);
		    if (error.response.data) {
		    	alert(error.response.data.errors[0])
		    }
		  });
	}, [])

	return (
		<div>
			<Header />
			<Post posts={posts} />
		</div>
	)
}


export default Home;