import {Container, Button, Form} from 'react-bootstrap';
import Header from '../components/header';
import axios from 'axios';
import {useState} from 'react';
import baseUrl from '../constants/baseUrl';
// import { useHistory } from "react-router-dom";
import { createBrowserHistory } from 'history';

const history = createBrowserHistory({ forceRefresh: true });

const LoginForm = () => {
	// const history = useHistory();

	const [email, setEmail]       = useState('');
	const [password, setPassword] = useState('');
	const [isLogin, setIsLogin]   = useState(true);

	const handleLogin = (event) => {
		event.preventDefault();
		console.log('ssfsf')
		console.log(email, password)
		
		if (isLogin) {
			axios.post(`${baseUrl}/user/login`, {
			    email,
			    password
			  })
			  .then(function (response) {
			    console.log('response == ', response);
			    let email = response.data.data.email;
			    localStorage.setItem("token", response.data.data.token);
			    localStorage.setItem("email", response.data.data.email);
				history.push("/home");
			  })
			  .catch(function (error) {
			    if (error.response.data) {
			    	alert(error.response.data.errors[0])
			    }
			  });

		} else {
			axios.post(`${baseUrl}/user/register`, {
			    email,
			    password
			  })
			  .then(function (response) {
			    console.log('response == ', response.data.message);
			    setIsLogin(true);
			    alert(response.data.message);
			  })
			  .catch(function (error) {
			    if (error.response.data) {
			    	alert(error.response.data.errors[0])
			    }
			  });
		}
	}


	return (
		<div>
			<Header />
			<br />
			<Container>
				<Form onSubmit={handleLogin}>
				  <Form.Group className="mb-3" controlId="formBasicEmail">
				    <Form.Label>Email address</Form.Label>
				    <Form.Control onChange={(e) => setEmail(e.target.value)} value={email} type="email" placeholder="Enter email" name="email" required />
				    <Form.Text className="text-muted">
				      We'll never share your email with anyone else.
				    </Form.Text>
				  </Form.Group>

				  <Form.Group className="mb-3" controlId="formBasicPassword">
				    <Form.Label>Password</Form.Label>
				    <Form.Control onChange={(e) => setPassword(e.target.value)} value={password} type="password" placeholder="Password" name="password" required />
				  </Form.Group>
				  
				  <Button variant="primary" type="submit">
				    {isLogin ? "Login" : "Register"}
				  </Button> &nbsp; &nbsp;
				  or &nbsp; &nbsp; <Button variant="info" onClick={() => setIsLogin(!isLogin)}>
				    	{isLogin ? "Register" : "Login"}
				  	 </Button>
				</Form>
			</Container>
		</div>
	);
};

export default LoginForm