import {Modal, Button} from 'react-bootstrap'
import {useState} from 'react';
import baseUrl from '../constants/baseUrl';
import axios from 'axios';
import { createBrowserHistory } from 'history';

const history = createBrowserHistory({ forceRefresh: true });

const OpenModal = ({show, handleClose}) => {
  	let token = localStorage.getItem("token");

	const [file, setFile] = useState(null);

	const uploadFile = () => {
		const formData = new FormData();

		formData.append( 
	        "file", 
	        file, 
	        file.name 
	    ); 
     
	    // Details of the uploaded file 
	   console.log(file); 
     
       // Request made to the backend api 
       // Send formData object 
       axios.post(`${baseUrl}/post/upload-file`, formData, {headers: { Authorization: `Token ${token}` }})
       	.then(function (response) {
		    console.log('response == ', response);

		    history.push("/home");
		  })
		.catch(function (error) {
			console.log(error.response.data.errors)
		    if (error.response.data) {
		    	alert(error.response.data.errors)
		    }
		  });
	}

	return (
		<Modal show={show} onHide={handleClose}>
	        <Modal.Header closeButton>
	          <Modal.Title>JSON File upload</Modal.Title>
	        </Modal.Header>
	        <Modal.Body>
	        	<input type='file' accept="application/JSON" onChange={(e) => setFile(e.target.files[0])} />
	        </Modal.Body>
	        <Modal.Footer>
	          <Button variant="primary" onClick={uploadFile} disabled={!file}>
	            Upload File
	          </Button>
	        </Modal.Footer>
	    </Modal>
	)
}

export default OpenModal