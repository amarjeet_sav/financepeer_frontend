import {Navbar, Nav, Container, Button} from 'react-bootstrap'
import axios from 'axios';
import baseUrl from '../constants/baseUrl';
import { useHistory } from "react-router-dom";
import {useState} from 'react';
import OpenModal from './uploadModal';
import { createBrowserHistory } from 'history';

const history = createBrowserHistory({ forceRefresh: true });

const Header = () => {
  const [openModal, setOpenModal] = useState(false);

  // let history = useHistory({ forceRefresh: true });

  let token = localStorage.getItem("token");
  let email = localStorage.getItem("email");

  const logout = () => {
    console.log('token = ', token)
    axios.post(`${baseUrl}/user/logout`, {}, {headers: { Authorization: `Token ${token}` }})
      .then(function (response) {
        console.log('response == ', response);
        
        localStorage.removeItem("token");
        localStorage.removeItem("email");

        history.push("/");
      })
      .catch(function (error) {
        console.log('error == ', error);
      });
  }

  const upload = () => {
    setOpenModal(true);


  }

  return (
    <>
    <Navbar sticky="top" style={{boxShadow: '0px 7px 15px rgb(0 0 0 / 10%)', backgroundColor: '#ffffff'}}>
      <Container fluid>
        <Navbar.Brand>
          <img
            alt=""
            src="https://d18gf9zcxp8qg0.cloudfront.net/newWebsite/Financepeer_new_logo.png"
            height="60"
            className="d-inline-block align-top"
          />{' '}
        </Navbar.Brand>
        {token && <>
          <Nav>
            {email && email}  &nbsp;&nbsp;&nbsp;
            <Button onClick={upload} >Upload</Button> &nbsp;&nbsp;&nbsp;
            <Button variant="danger" onClick={logout} >Logout</Button>
          </Nav>
          </>}
      </Container>
    </Navbar>
    {openModal && <OpenModal show={openModal} handleClose={() => setOpenModal(false)}/>}
    </>
  )
}

export default Header;