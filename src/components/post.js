import {Table, Pagination} from 'react-bootstrap'

const Post = (props) => {
	console.log('props == ', props.posts ? "amar": "sav")
	return (
			props.posts.length > 1 ? 
				<Table striped bordered hover responsive>
					<thead>
						<tr>
							<th>ID</th>
							<th>User ID</th>
							<th>Title</th>
							<th>Body</th>
						</tr>
					</thead>
					<tbody>
						{props.posts.map((post, i) => (
							<tr key={i}>
								<td>{post.post_id}</td>
								<td>{post.user_id}</td>
								<td>{post.title}</td>
								<td>{post.body}</td>
							</tr>
						))}
					</tbody>
				</Table>
			:
			<h1 style={{textAlign: "center", lineHeight: "400px"}}>Data Not Available</h1>
	)
}

export default Post;